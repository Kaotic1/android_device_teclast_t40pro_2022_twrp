# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)

# Inherit from T40Pro_2022_EEA device
$(call inherit-product, device/teclast/T40Pro_2022_EEA/device.mk)

# Inherit some common twrp stuff.
$(call inherit-product, vendor/twrp/config/common.mk)

# Device identifier. This must come after all inclusions
PRODUCT_DEVICE := T40Pro_2022_EEA
PRODUCT_NAME := twrp_T40Pro_2022_EEA
PRODUCT_BRAND := Teclast
PRODUCT_MODEL := Teclast T40 Pro
PRODUCT_MANUFACTURER := teclast
PRODUCT_SHIPPING_API_LEVEL := 31

PRODUCT_GMS_CLIENTID_BASE := android-incar

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="T40Pro_2022_EEA-user 12 SP1A.210812.016 221010 release-keys"

BUILD_FINGERPRINT := Teclast/T40Pro_2022_EEA/T40Pro_2022_EEA:12/SP1A.210812.016/221010:user/release-keys