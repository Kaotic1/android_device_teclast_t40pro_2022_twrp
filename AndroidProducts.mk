PRODUCT_MAKEFILES := \
     $(LOCAL_DIR)/twrp_T40Pro_2022_EEA.mk

COMMON_LUNCH_CHOICES := \
    twrp_T40Pro_2022_EEA-user \
    twrp_T40Pro_2022_EEA-userdebug \
    twrp_T40Pro_2022_EEA-eng
